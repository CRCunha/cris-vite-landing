import { makeStyles } from '@material-ui/styles';
import { shadow } from '@/configs/colors';
import BgImage from '@/assets/bg.png';

const useStyles = makeStyles((theme) => ({
  headerContainer: {
    height: '100vh',
    backgroundImage: `url(${BgImage})`,
    backgroundPosition: 'center',
    backgroundSize: 'cover',
  },
}));

export default useStyles;
