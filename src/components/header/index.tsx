import React from 'react';
import useStyles from './styles';
import { Grid, Button } from '@mui/material';
import NavBar from '@/components/navBar';

const Header = () => {
  const classes = useStyles();
  return (
    <Grid container justifyContent="center" className={classes.headerContainer}>
      <NavBar />
      <Grid className={classes.headerContent} item xs={11}></Grid>
    </Grid>
  );
};

export default Header;
