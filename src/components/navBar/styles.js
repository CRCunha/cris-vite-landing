import { makeStyles } from '@material-ui/styles';
import { shadow } from '@/configs/colors';

const useStyles = makeStyles((theme) => ({
  navBarContainer: {
    height: 80,
  },

  navBarContent: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',

    '& img': {
      width: 60,
    },
  },
}));

export default useStyles;
