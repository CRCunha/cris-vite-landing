import useStyles from './styles';
import { Grid, Button } from '@mui/material';
import Logo from '@/assets/logo.png';

const NavBar = () => {
  const classes = useStyles();
  return (
    <Grid container justifyContent="center" className={classes.navBarContainer}>
      <Grid className={classes.navBarContent} item xs={11}>
        <img src={Logo} alt="Logo" />
        <Button variant="outlined" disableElevation>
          Login
        </Button>
      </Grid>
    </Grid>
  );
};

export default NavBar;
