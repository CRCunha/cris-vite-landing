import { makeStyles } from '@material-ui/styles';
import BgImage from '@/assets/bg.png';

const useStyles = makeStyles((theme) => ({
  homeContainer: {
    width: '100%',
    backgroundImage: `url(${BgImage})`,
  },
}));

export default useStyles;
