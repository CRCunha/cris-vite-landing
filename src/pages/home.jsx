import React from 'react';
import Grid from '@mui/material/Grid';
import Header from '@/components/header';
import useStyles from './styles';

const homePage = () => {
  const classes = useStyles();
  return (
    <Grid container justifyContent="center">
      <div className={classes.homeContainer}>
        <Header />
      </div>
    </Grid>
  );
};

export default homePage;
